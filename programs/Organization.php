<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009-2019 by CANTICO ({@link http://www.cantico.fr})
 * @copyright Copyright (c) 2019 by Cap Welton ({@link https://www.capwelton.com})
 */

namespace Capwelton\App\Organization;

use Capwelton\App\Address;


/**
 * Organization object
 *
 * @property string					$name
 * @property string					$description
 * @property string					$activity
 * @property string                 $email
 * @property string                 $phone
 * @property string                 $fax
 * @property string                 $website
 * @property Organization		    $parent
 * @property Address\Address		$address
 */
class Organization extends \app_Record
{
}
