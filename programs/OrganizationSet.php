<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009-2019 by CANTICO ({@link http://www.cantico.fr})
 * @copyright Copyright (c) 2019 by Cap Welton ({@link https://www.capwelton.com})
 */

namespace Capwelton\App\Organization;

use Capwelton\App\Address;


/**
 * An Organization may be a company, association...
 *
 * @property ORM_StringField            $name
 * @property ORM_TextField              $description
 * @property ORM_StringField            $activity
 * @property ORM_EmailField             $email
 * @property ORM_StringField            $phone
 * @property ORM_StringField            $fax
 * @property ORM_UrlField               $website
 * @property OrganizationSet            $parent
 * @property OrganizationTypeSet        $type
 * @property Address\AddressSet         $address
 *
 * @method Organization                  get()
 * @method Organization                  request()
 * @method Organization[]|\ORM_Iterator  select()
 * @method Organization                  newRecord()
 */
class OrganizationSet extends \app_RecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);

        $App = $this->App();

        $this->setPrimaryKey('id');

        $this->setDescription('Organization');

        $this->addFields(
            ORM_StringField('name')
                    ->setDescription('Name'),
            ORM_TextField('description')
                    ->setDescription('Description'),
            ORM_TextField('logo')
                    ->setDescription('Logo'),
            ORM_StringField('activity')
                    ->setDescription('Main activity code'),
            ORM_EmailField('email')
                ->setDescription('Email'),
            ORM_StringField('phone')
                ->setDescription('Phone'),
            ORM_StringField('fax')
                ->setDescription('Fax'),
            ORM_UrlField('website')
                ->setDescription('Web site')
        );

        $this->hasOne('parent', $App->OrganizationSetClassName())
            ->setDescription('Parent organization');

        $this->hasOne('address', $App->AddressSetClassName())
            ->setDescription('Address');

        $this->hasOne('replacedBy', $App->OrganizationSetClassName()); // If the organization has been deleted and replaced by another organization.
    }
}

